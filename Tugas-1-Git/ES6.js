console.log('=====No.1=====')

const golden = goldenFunction = () => {
  console.log("this is golden!!")
}
 
golden() ;

console.log('=====No.2=====')

const newFunction = literal = (firstName, lastName)=>{
 return {
 firstName,
 lastName,
 fullName() {
 console.log(firstName, lastName)
 }
 }
 }
 literal("William", "Imoh").fullName()
 
 console.log('=====No.3=====')
 
 let newObject = {
  firstName: 'Harry',
  lastName: 'Potter Holt',
  destination: 'Hogwarts React Conf',
  occupation: 'Deve-wizard Avocado',
  spell: 'Vimulus Renderus!!!'
};
const { firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName) ;
console.log(lastName) ;
console.log(destination) ;
console.log(occupation) ;

 console.log('=====No.4=====')
 
 const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']


let combinedArray = [...west, ...east]
console.log(combinedArray)

 console.log('=====No.5=====')
 
 const planet = 'earth'
const view = 'glass'

const before = `${planet} ${view}`
console.log(before)

