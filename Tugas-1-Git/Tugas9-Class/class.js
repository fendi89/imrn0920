console.log('======No.1=====')
class Animal {
    constructor(name) {
        this.legs = 4;
        this.cold_blooded = false;
        this.name = name;
    }
    get cnam() {
        return this.name;
    }
    set cnam(x){
        this.name = x;
    }
}
var sheep = new Animal ("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('======release=====')

class frog extends Animal{
    constructor(name){
        super(name);
    }
    jump(){
        console.log('Hop hop')
    }
}

class Ape extends Animal{
    constructor(name){
        super(name);
    }
    yell(){
        console.log('Auooo')
    }
}

 
var kodok = new frog("buduk")
kodok.jump() // "hop hop" 

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

console.log('======No.2=====')


render = () =>{  

    let date = new Date()

    let hours = date.getHours()
    if (hours < 10 ) hours = '0' + hours

    let mins = date.getMinutes()
    if (mins < 10 ) mins = '0' + mins

    let secs = date.getSeconds()
    if (secs < 10 ) secs = '0' + secs

    let output = this.template
    .replace('h', hours)
    .replace('m', mins)
    .replace('s', secs)

    console.log(output)
}

