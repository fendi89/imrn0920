console.log("=====No.1=====")

function range(num1, num2){
    let nilarray=[];
    if (num1 <= num2){
        for (let i=num1; i<=num2; i++){
            nilarray.push(i);
        }
    }else if (num1>=num2){
        for (let i=num1; i>=num2; i--){
            nilarray.push(i);
        }
    }else{
        nilarray.push(-1);
    }
    return nilarray;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) //[11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("=====No.2=====")
function rangeWithStep (startnum, finishnum, step) {
    let numberstep =[]
    if (startnum <= finishnum) {
        for (let i= startnum; i <= finishnum; i=i+step){
            numberstep.push(i);
        }
    }else if (startnum>=finishnum){
        for (let i=startnum; i>=finishnum; i=i-step){
            numberstep.push(i);
        }
    }else{
        numberstep.push(-1);
    }
    return numberstep;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("=====No.3=====")

function sum (startnum, finishnum, step){
  var rangeArr =[];
  var distance;

  if(!step){
    distance=1
  }else{
    distance = step
  }
  if (startnum > finishnum){
    var currentNum = startnum;
    for( var i=0; currentNum >= finishnum; i++){
      rangeArr.push(currentNum)
      currentNum -= distance
    }
  }else if (startnum < finishnum){
    var currentNum = startnum;
    for(var i=0; currentNum <= finishnum; i++){
        rangeArr.push(currentNum)
        currentNum += distance
    }
  }else if (!startnum && !finishnum && !step){
      return 0

  }else if (startnum){
      return startnum
  }

  var total = 0;
  for (var i= 0; i< rangeArr.length; i++){
  total = total + rangeArr[i]
}
return total
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("=====No.4=====")

var input = [
            ["0001", "Roman Alamsyah", "Bandar Lampung","21/05/1989","Membaca"],
            ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
            ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
            ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
 ]           
function dataHandling(data){
var dataLength = data.length
for(var i=0; i<dataLength; i++){
var id = "Nomor id :" +data[i][0]
var nama = "Nama Lengkap :" +data[i][1]
var ttl = "TTL : " +data[i][2] + " " + data[i][3]
var hobi = "Hobi :" + data[i][4]

console.log(id)
console.log(nama)
console.log(ttl)
console.log(hobi)
}
}
dataHandling(input)

console.log("=====No.5=====")

function balikKata(kata){
  var balik = "";
  for( var i=kata.length-1; i>=0; i--)
  {
    balik += kata[i]
  }
  return balik;
}
console.log(balikKata("kasur"))

console.log("=====No.6=====")

var input =["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input);

function dataHandling2(data){
    var newData = data
    var newName = data[1] + "Elsharawy"
    var newProvince = "Provinsi" + data[2]
    var gender = "pria"
    var institusi = "SMA International Metro"

    newData.splice(1, 1, newName)
    newData.splice(2, 1, newProvince)
    newData.splice(4, 1, gender, institusi)

    var arrDate = data [3]
    var newDate = arrDate.split('/')
    var monthNum = newData[1]
    var monthname = " "

    switch (monthNum){
        case "01":
            monthname = "Januari"
            break;
        case "02":
            monthname = "Februari"
            break;
        case "03":
            monthname = "Maret"
            break;
        case "04":
            monthname = "April"
            break;
        case "05":
            monthname = "Mei"
            break;
        case "06":
            monthname = "Juni"
            break;
        case "07":
            monthname = "Juli"
            break;
        case "08":
            monthname = "Agustus"
            break;
        case "09":
            monthname = "September"
            break;
        case "10":
            monthname = "Oktober"
            break;
        case "11":
            monthname = "November"
            break;
        case "12":
            monthname = "Desember"
            break;
        default:
            break;

    }
    var dateJoin = newDate.join("-")
    var dateArr = newDate.sort(function(value1, value2){
        value2- value1
    })
    var editName=newName.slice(0, 15)
    console.log(newData)
    console.log(monthname)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
}
